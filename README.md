# Citadel

Citadel is now maintained as part of Nirvati.

Please check

- [here for Citadel 0.3 LTS](https://gitlab.com/nirvati/citadel/lts)
- [here for Citadel 0.4 (in development)](https://gitlab.com/nirvati/citadel)
